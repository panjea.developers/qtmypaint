#!/usr/bin/env python3

import os, sys, subprocess, ctypes

qmake = 'qmake'
if os.path.isfile('/usr/local/opt/qt@5/bin/qmake'):
	qmake = '/usr/local/opt/qt@5/bin/qmake'

dllpath = './demo/libqtmypaint.dylib'

if '--build' in sys.argv or not os.path.isfile(dllpath):
	subprocess.check_call([qmake])
	subprocess.check_call(['make'])


dll = ctypes.CDLL(dllpath)
print(dll)
dll.qtmypaint_main(0)